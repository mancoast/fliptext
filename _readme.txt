primary object: convert regular text into flip text to demonstart .net programming ability
can you see the beast behind changing letters?  Only some do... lets demonstrate
<this> basic C# user interface utilizes an editable richtext box form for user input
Output is sent to a static label where it can easily be copied to the clipboard

functional specs:
	wait for user to input text
	parse text, flip, output to label
technical specs:
	wait for an event indicating richtextbox text has change
	read text into a temp string variable
		a string is simply an array or set of character variables
	create a vector of character variables
		vectors are similar to arrays, but much more powerful
		vectors operate similar to a stack of paper
		objects are added on top of the stack (push)
		objects can be removed from the top of the stack (pop)
		objects can be removed from any position by specifying an iterator (accessor)
	read each character from the string in sequential order
		strings are arrays, which natively support an accessor to any character
		simple for loop will accomplish this task without effort
	convert each character to its flipped version
		logic of the program
		any characters without a flipped counter-part are not affected
	push each character to the vector in sequential order
		adds the first letter in the text box at the bottom of the stack
		last letter is on the very top of the stack
	clear the temp string variable
	pop each character from the vector
		this removes the most recently added
		character are removed and processed in reverse
		advantageous because upside-down text is read right to left across the page
	Concatenate each popped character to the temp string
	output temp string to label