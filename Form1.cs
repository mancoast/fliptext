﻿//standard headers
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
namespace d_lɟ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            string path = @"d!lɟ.txt";
            if (!File.Exists(path))
            {
                string createText = "dominate4eva" + Environment.NewLine + "nothing can stop" + Environment.NewLine;
                File.WriteAllText(path, createText);
            }
            string contents = System.IO.File.ReadAllText(path);
            this.richTextBox1.Text = contents;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            string path = @"d!lɟ.txt";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            string createText = this.richTextBox1.Text;
            File.WriteAllText(path, createText);
            SaveFlip();
        }

        private void SaveFlip()
        {
            string path = @"d!lɟ-flip.txt";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            string createText = this.richTextBox2.Text;
            File.WriteAllText(path, createText);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            string temp = this.richTextBox1.Text;
            Stack myStack = new Stack();
            foreach (char tmp1 in temp)
                myStack.Push(flip(tmp1));
            temp = "";
            foreach (char tmp2 in myStack)
                temp = temp + tmp2;
            this.richTextBox2.Text = temp;
            richTextBox2.SelectAll();
            richTextBox2.SelectionAlignment = HorizontalAlignment.Right;
        }

        private char flip(char letter)
        {
            switch (letter)
            {
                case 'A':
                case 'a':
                    {
                        return 'ɐ';
                    }
                case 'B':
                case 'b':
                    {
                        return 'q';
                    }
                case 'C':
                case 'c':
                    {
                        return 'ɔ';
                    }
                case 'D':
                case 'd':
                    {
                        return 'p';
                    }
                case 'E':
                case 'e':
                    {
                        return 'ǝ';
                    }
                case 'F':
                case 'f':
                    {
                        return 'ɟ';
                    }
                case 'G':
                case 'g':
                    {
                        return 'ƃ';
                    }
                case 'H':
                case 'h':
                    {
                        return 'ɥ';
                    }
                case 'I':
                case 'i':
                    {
                        return '!';
                    }
                case 'J':
                case 'j':
                    {
                        return 'ɾ';
                    }
                case 'K':
                case 'k':
                    {
                        return 'ʞ';
                    }
                case 'L':
                case 'l':
                    {
                        return 'l';
                    }
                case 'M':
                case 'm':
                    {
                        return 'ɯ';
                    }
                case 'N':
                case 'n':
                    {
                        return 'u';
                    }
                case 'O':
                case 'o':
                    {
                        return 'o';
                    }
                case 'P':
                case 'p':
                    {
                        return 'd';
                    }
                case 'Q':
                case 'q':
                    {
                        return 'b';
                    }
                case 'R':
                case 'r':
                    {
                        return 'ɹ';
                    }
                case 'S':
                case 's':
                    {
                        return 's';
                    }
                case 'T':
                case 't':
                    {
                        return 'ʇ';
                    }
                case 'U':
                case 'u':
                    {
                        return 'n';
                    }
                case 'V':
                case 'v':
                    {
                        return 'ʌ';
                    }
                case 'W':
                case 'w':
                    {
                        return 'ʍ';
                    }
                case 'X':
                case 'x':
                    {
                        return 'x';
                    }
                case 'Y':
                case 'y':
                    {
                        return 'ʎ';
                    }
                case 'Z':
                case 'z':
                    {
                        return 'z';
                    }
                default:
                    {
                        return letter;
                    }
            }
        }
    }
}
